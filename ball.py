import pygame
import matplotlib.pyplot as plt
import random
import numpy as np

c_screenWidth = 800
c_screenHeight = 600

# visualization consts
c_meterToPixel = 1000

# Define the colors we will use in RGB format
BLUE = (50, 50, 255)
GRAY = (200, 200, 200)

pygame.init()
font = pygame.font.SysFont("Verdana", 12)
screen = pygame.display.set_mode((c_screenWidth, c_screenHeight))
pygame.display.set_caption("Template Simulation")
clock = pygame.time.Clock()

def MetersToPixels(meters):
    return round(meters * c_meterToPixel)

class Environment:
    def __init__(self):
        self.exampleObject = Ball(0, 0.4, 0.08, 5)
        self.sensor = Sensor(self)
        self.reference = Reference()
        self.controller = Controller(self.reference, self.sensor)
        self.system = System(self.controller)

    def Update(self, elapsedTime):
        self.reference.Update(elapsedTime)
        self.sensor.Update()
        self.controller.Update(elapsedTime)
        self.system.Update(elapsedTime)

    def Visualize(self):
        self.exampleObject.Draw()
        self.system.Draw()

class System:
    def __init__(self, controller):
        self.controller = controller

    def Update(self, elapsedTime):
        pass

    def Draw(self):
        pass

class Sensor:
    def __init__(self, environment):
        self.environment = environment
        self.measurement = 0.0
        self.bias = 0.0
        self.sigma = 0.003

    def Update(self):
        noise = random.gauss(self.bias, self.sigma)
        # self.measurement = enviroment._something_you_observe + noise

class Controller:
    def __init__(self, reference, sensor):
        self.reference = reference
        self.sensor = sensor
        self.controlSignal = 0.0

    def Update(self, elapsedTime):
        pass
        # self.controlSignal =

class Reference:
    def __init__(self):
        self.time = 0.0
        self.targetValue = 0.0
        self.lastUpdateTime = 0.0
        self.updatePeriod = 3.0
    
    def Update(self, elapsedTime):
        self.time += elapsedTime
        if (self.time - self.lastUpdateTime) > self.updatePeriod:
            self.targetValue = random.uniform(-0.2, 0.2)
            self.lastUpdateTime = self.time

    def Draw(self, beam):
        pass

class Ball:
    
    def __init__(self, x, y, r, m):
        self.x = round(c_screenWidth/2 + MetersToPixels(x))
        self.y = 0.8 - y  #Hardkodovana visina
        self.r = r
        self.m = m
        self.totalforce = 0
        self.v = 0
        self.area = 4*self.r**2*np.pi 
        self.drag_coeff = 1

    def Draw(self):
        pygame.draw.circle(screen, BLUE, (self.x,MetersToPixels(self.y)), MetersToPixels(self.r), 0)

    
    def Move(self, elapsedTime):
        a = self.totalforce/self.m
        v = self.v + a * elapsedTime
        y = self.x + self.v * elapsedTime   #Koristili smo prvu integraciju

    def updateTotalForce(self, v, Rho, g):
        self.totalforce = (1+self.drag_coeff)*Rho* (self.area/2) * (v^2)/2 - self.m * g

class Propeler():

    def __init__(self):
        self.napon = 0
        self.coeff = 0.3
        self.brzina = 0
        #Nacrtati propeler

    def updateNapon(self, newnapon):
        self.brzina = newnapon * self.coeff
    
    

class Logger:
    def __init__(self):
        self.targetValue = []
        self.measuredOutput = []
        self.controlSignal = []
        self.time = []

    def Update(self, env, elapsedTime):
        if (len(self.time) == 0):
            self.time.append(elapsedTime)
        else:
            self.time.append(self.time[-1] + elapsedTime)
        self.targetValue.append(env.reference.targetValue)
        self.controlSignal.append(env.controller.controlSignal)
        self.measuredOutput.append(env.sensor.measurement)

    def Draw(self):
        plt.plot(self.time, self.targetValue)
        plt.plot(self.time, self.measuredOutput, '--')
        plt.xlabel('time [s]')
        plt.legend(['target', 'measurement'])
        plt.show()
        plt.plot(self.time, self.controlSignal)
        plt.xlabel('time [s]')
        plt.ylabel('control signal')
        plt.show()

def Simulation():
    environment = Environment()
    logger = Logger()

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # clear the screen every time before drawing new objects  
        screen.fill(GRAY)

        dt = clock.tick(60) / 1000
        environment.Update(dt)
        if running:
            logger.Update(environment, dt)
            environment.Visualize()
            pygame.display.flip()
        else:
            logger.Draw()
            break

if __name__ == '__main__':
    Simulation()